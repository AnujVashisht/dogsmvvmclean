package com.anuj.mvvmcleandemo.feature.entry

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.anuj.mvvmcleandemo.domain.DogSearchUseCase
import com.anuj.mvvmcleandemo.R
import com.anuj.mvvmcleandemo.TestCoroutineRule
import com.anuj.mvvmcleandemo.domain.domainmodel.BreedDetails
import com.anuj.mvvmcleandemo.domain.domainmodel.ImageDetails
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import java.net.UnknownHostException

@ExperimentalCoroutinesApi
class EntryViewModelTest {

    private var dogSearchUseCase = mockk<DogSearchUseCase>()
    private lateinit var viewModel: EntryViewModel

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @ExperimentalCoroutinesApi
    @Before
    fun setup() {
        viewModel = EntryViewModel(dogSearchUseCase)
    }

    @Test
    fun `when response is empty list, should start-end loading and set empty list on view`() {
        val mockedObserver = createMockObserver()
        coEvery { dogSearchUseCase.searchDog(any()) } returns emptyList()
        viewModel.state.observeForever(mockedObserver)

        testCoroutineRule.runBlockingTest {
            viewModel.searchDog("hound")

            val slots = mutableListOf<EntryViewState>()
            verify { mockedObserver.onChanged(capture(slots)) }
            assertTrue(slots[0].isLoading == false)
            assertTrue(slots[1].isLoading == true)
            assertTrue(slots[2].isLoading == false)
            assertTrue(slots[2].searchResults?.isEmpty() == true)
        }
    }

    @Test
    fun `when response is non-empty list, should start-end loading and set list on view`() {
        val mockedObserver = createMockObserver()
        val response = listOf(
            BreedDetails(
                "id",
                "test name",
                "",
                "",
                null,
                imageDetails = null,
                "",
                ""
            )
        )
        coEvery { dogSearchUseCase.searchDog(any()) } returns response
        viewModel.state.observeForever(mockedObserver)

        testCoroutineRule.runBlockingTest {
            viewModel.searchDog("hound")

            val slots = mutableListOf<EntryViewState>()
            verify { mockedObserver.onChanged(capture(slots)) }
            assertTrue(slots[0].isLoading == false)
            assertTrue(slots[1].isLoading == true)
            assertTrue(slots[2].isLoading == false)
            assertTrue(slots[2].searchResults?.get(0)?.name == "test name")
        }
    }

    @Test
    fun `when response is non-empty list with images, should fetch image in background`() {
        val mockedObserver = createMockObserver()
        val response = listOf(
            BreedDetails(
                "id",
                "test name",
                "",
                "",
                "",
                imageDetails = null,
                "",
                ""
            )
        )
        coEvery { dogSearchUseCase.getImage(any()) } returns ImageDetails("test url")
        coEvery { dogSearchUseCase.searchDog(any()) } returns response
        viewModel.state.observeForever(mockedObserver)

        testCoroutineRule.runBlockingTest {
            viewModel.searchDog("hound")

            val slots = mutableListOf<EntryViewState>()
            verify { mockedObserver.onChanged(capture(slots)) }
            assertTrue(slots[0].isLoading == false)
            assertTrue(slots[1].isLoading == true)
            assertTrue(slots[2].isLoading == false)
            assertTrue(slots[2].searchResults?.get(0)?.name == "test name")
            assertTrue(slots[2].searchResults?.get(0)?.imageDetails?.url == "test url")
        }
    }

    @Test
    fun `when no text is entered, should not search anything`() {
        testCoroutineRule.runBlockingTest {
            val mockedObserver = createMockObserver()
            viewModel.state.observeForever(mockedObserver)

            viewModel.onSearchTextChanged("",0)

            val slots = mutableListOf<EntryViewState>()
            verify { mockedObserver.onChanged(capture(slots)) }
            assertTrue(slots[0].isLoading == false)
            assertTrue(slots[0].searchResults == null)
        }
    }

    @Test
    fun `when one char is entered, should not search anything`() {
        testCoroutineRule.runBlockingTest {
            val mockedObserver = createMockObserver()
            viewModel.state.observeForever(mockedObserver)

            viewModel.onSearchTextChanged("a",1)

            val slots = mutableListOf<EntryViewState>()
            verify { mockedObserver.onChanged(capture(slots)) }
            assertTrue(slots[0].isLoading == false)
            assertTrue(slots[0].searchResults == null)
        }
    }

    @Test
    fun `when more char is entered, should trigger search`() {
            val mockedObserver = createMockObserver()
            viewModel.state.observeForever(mockedObserver)
            coEvery { dogSearchUseCase.searchDog(any()) } returns emptyList()

            testCoroutineRule.runBlockingTest {
                viewModel.onSearchTextChanged("abc",3)
            }

            val slots = mutableListOf<EntryViewState>()
            verify { mockedObserver.onChanged(capture(slots)) }
            assertTrue(slots[0].isLoading == false)
            assertTrue(slots[0].searchResults == null)
            assertTrue(slots[1].isLoading == true)
            assertTrue(slots[0].searchResults == null)
            assertTrue(slots[2].isLoading == false)
            assertTrue(slots[2].searchResults?.isEmpty() == true)
    }

    @Test
    fun `when internet error, should return appropriate message to the user`() {
        val mockedObserver = createMockObserver()
        viewModel.state.observeForever(mockedObserver)
        coEvery { dogSearchUseCase.searchDog(any()) } throws UnknownHostException("Error")

        testCoroutineRule.runBlockingTest {
            viewModel.searchDog("")
        }
        val slots = mutableListOf<EntryViewState>()
        verify { mockedObserver.onChanged(capture(slots)) }
        assertTrue(slots[0].isLoading == false)
        assertTrue(slots[0].searchResults == null)
        assertTrue(slots[1].isLoading == true)
        assertTrue(slots[0].searchResults == null)
        assertTrue(slots[2].isLoading == false)
        assertTrue(slots[2].error?.message == R.string.error_check_internet_connection)
    }

    @Test
    fun `when other error, should return appropriate message to the user`() {
        val mockedObserver = createMockObserver()
        viewModel.state.observeForever(mockedObserver)
        coEvery { dogSearchUseCase.searchDog(any()) } throws Exception("Error")

        testCoroutineRule.runBlockingTest {
            viewModel.searchDog("")
        }
        val slots = mutableListOf<EntryViewState>()
        verify { mockedObserver.onChanged(capture(slots)) }
        assertTrue(slots[0].isLoading == false)
        assertTrue(slots[0].searchResults == null)
        assertTrue(slots[1].isLoading == true)
        assertTrue(slots[0].searchResults == null)
        assertTrue(slots[2].isLoading == false)
        assertTrue(slots[2].error?.message == R.string.error_oops_error_occured)
    }

    @Suppress("ObjectLiteralToLambda")
    fun createMockObserver(): Observer<EntryViewState> {
        val observer = object : Observer<EntryViewState> {
            override fun onChanged(t: EntryViewState?) {}
        }
        return spyk<Observer<EntryViewState>>(observer)
    }
}