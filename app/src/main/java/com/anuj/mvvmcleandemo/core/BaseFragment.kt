package com.anuj.mvvmcleandemo.core

import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding

open class BaseFragment<Binding: ViewBinding> : Fragment() {
    internal var _binding: Binding? = null
    internal val binding get() = _binding!!

}