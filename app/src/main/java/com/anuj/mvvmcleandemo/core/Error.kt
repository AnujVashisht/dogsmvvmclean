package com.anuj.mvvmcleandemo.core

import androidx.annotation.StringRes

data class Error(@StringRes val message: Int)