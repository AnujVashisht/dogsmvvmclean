package com.anuj.mvvmcleandemo.model

import android.os.Parcelable
import com.anuj.mvvmcleandemo.domain.domainmodel.BreedDetails
import kotlinx.parcelize.Parcelize

@Parcelize
data class PclBreedDetails(
    val id: String,
    val name: String,
    val temperament: String?,
    val lifeSpan: String,
    val url: String?,
    val wikiLink: String?,
    val origin: String?
) : Parcelable

fun BreedDetails.toParcelable(): PclBreedDetails {
    return PclBreedDetails(id, name, temperament, lifeSpan, imageDetails?.url, wikipediaUrl, origin)
}