package com.anuj.mvvmcleandemo.feature.entry

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.anuj.mvvmcleandemo.core.BaseFragment
import com.anuj.mvvmcleandemo.databinding.FragmentEntryBinding
import com.anuj.mvvmcleandemo.domain.domainmodel.BreedDetails
import com.anuj.mvvmcleandemo.model.toParcelable
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class EntryFragment : BaseFragment<FragmentEntryBinding>() {

    private val entryViewModel by viewModels<EntryViewModel>()
    private val searchResultsAdapter = SearchResultsAdapter(::onListItemClicked)
    private val defaultSearchString = "American"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentEntryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            entryViewModel.searchDog(defaultSearchString)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.apply {
            adapter = searchResultsAdapter
            layoutManager = LinearLayoutManager(requireContext())
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    (layoutManager as LinearLayoutManager).orientation
                )
            )
        }
        binding.searchField.doOnTextChanged { text, _, _, count ->
            entryViewModel.onSearchTextChanged(text.toString(), count)
        }
        if (savedInstanceState == null) {
            entryViewModel.state.observe(viewLifecycleOwner, {
                it.error?.run {
                    Snackbar.make(binding.root, this.message, Snackbar.LENGTH_SHORT).apply {
                        animationMode = Snackbar.ANIMATION_MODE_SLIDE
                        show()
                    }
                }
                it.isLoading?.run {
                    binding.progressView.visibility = if (this) View.VISIBLE else View.INVISIBLE
                }
                it.searchResults?.run {
                    searchResultsAdapter.setData(this)
                }
            })
        }
    }

    private fun onListItemClicked(breedDetails: BreedDetails) {
        findNavController().navigate(
            EntryFragmentDirections.actionEntryFragmentToBreedDetailsFragment(
                breedDetails.toParcelable()
            )
        )
    }
}