package com.anuj.mvvmcleandemo.feature.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.anuj.mvvmcleandemo.R
import com.anuj.mvvmcleandemo.core.BaseFragment
import com.anuj.mvvmcleandemo.databinding.FragmentBreedDetailsBinding
import com.bumptech.glide.RequestManager
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class BreedDetailsFragment: BaseFragment<FragmentBreedDetailsBinding>() {

    @Inject lateinit var glide: RequestManager
    private val args by navArgs<BreedDetailsFragmentArgs>()
    private val breedDetailsViewModel by viewModels<BreedDetailsViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentBreedDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        breedDetailsViewModel.state.observe(viewLifecycleOwner, {
            it.breedDetails?.let { details ->
                binding.apply {
                    breedName.text = details.name
                    val context = requireContext()
                    lifeSpan.text = context.getString(R.string.age_string, details.lifeSpan)
                    temperament.text = context.getString(R.string.temperament_string, details.temperament)
                    wikiLink.text = details.wikiLink
                    origin.text = details.origin
                    details.url?.let { url ->
                        glide.load(url).into(image)
                    }
                }
            }
        })
        breedDetailsViewModel.renderDetails(args)
    }
}