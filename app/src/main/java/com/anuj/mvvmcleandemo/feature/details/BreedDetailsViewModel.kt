package com.anuj.mvvmcleandemo.feature.details

import com.anuj.mvvmcleandemo.core.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import javax.inject.Inject

@HiltViewModel
class BreedDetailsViewModel @Inject constructor():
    BaseViewModel<BreedDetailsViewState>() {
    override val coroutineExceptionHandler: CoroutineExceptionHandler
        get() = TODO("Not yet implemented")

    init {
        _state.value = BreedDetailsViewState(null)
    }

    fun renderDetails(args: BreedDetailsFragmentArgs) {
        _state.value = _state.value?.copy(breedDetails = args.breedDetails)
    }
}