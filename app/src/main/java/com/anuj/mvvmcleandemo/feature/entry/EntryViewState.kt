package com.anuj.mvvmcleandemo.feature.entry

import com.anuj.mvvmcleandemo.core.Error
import com.anuj.mvvmcleandemo.domain.domainmodel.BreedDetails

data class EntryViewState (
    val isLoading: Boolean?,
    val error: Error?,
    val searchResults: List<BreedDetails>?
)
