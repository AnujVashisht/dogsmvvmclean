package com.anuj.mvvmcleandemo.feature.entry

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.anuj.mvvmcleandemo.R
import com.anuj.mvvmcleandemo.databinding.ItemSearchViewBinding
import com.anuj.mvvmcleandemo.domain.domainmodel.BreedDetails
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import javax.inject.Inject

class SearchResultsAdapter(private val callback: (BreedDetails) -> Unit) :
    RecyclerView.Adapter<SearchViewHolder>() {
    @Inject
    lateinit var glide: RequestManager
    private val _dogBreeds = mutableListOf<BreedDetails>()

    @SuppressLint("NotifyDataSetChanged")
    fun setData(dogBreeds: List<BreedDetails>) {
        _dogBreeds.clear()
        _dogBreeds.addAll(dogBreeds)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        val binding = ItemSearchViewBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return SearchViewHolder(binding, callback)
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        holder.bind(_dogBreeds[position])
    }

    override fun getItemCount() = _dogBreeds.size
}

class SearchViewHolder(
    private val binding: ItemSearchViewBinding,
    private val callback: (BreedDetails) -> Unit
) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(breedDetails: BreedDetails) {
        val context = binding.root.context
        binding.apply {
            breedName.text = breedDetails.name
            lifeSpan.text = context.getString(R.string.age_string, breedDetails.lifeSpan)
            temperament.text = context.getString(
                R.string.temperament_string,
                breedDetails.temperament
            )
            breedDetails.imageDetails?.url.run {
                Glide.with(context).load(this).placeholder(R.drawable.ic_no_image)
                    .into(dogImage)
            }
            root.setOnClickListener {
                callback(breedDetails)
            }
        }
    }
}