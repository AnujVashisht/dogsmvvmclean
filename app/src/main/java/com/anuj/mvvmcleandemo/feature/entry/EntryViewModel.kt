package com.anuj.mvvmcleandemo.feature.entry

import com.anuj.mvvmcleandemo.domain.DogSearchUseCase
import com.anuj.mvvmcleandemo.core.BaseViewModel
import com.anuj.mvvmcleandemo.core.Error
import com.anuj.mvvmcleandemo.core.ExceptionHandler
import com.anuj.mvvmcleandemo.domain.domainmodel.BreedDetails
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.delay
import javax.inject.Inject

@HiltViewModel
class EntryViewModel @Inject constructor(private val dogSearchUseCase: DogSearchUseCase) :
    BaseViewModel<EntryViewState>() {

    private var searchJob: Job? = null
    private val debounceDelay = 500L
    private val minimumCharacters = 1

    override val coroutineExceptionHandler = CoroutineExceptionHandler { _, exception ->
        val message = ExceptionHandler.parse(exception)
        _state.value = _state.value?.copy(error = Error(message), isLoading = false)
    }

    init {
        _state.value = EntryViewState(false, null, null)
    }

    fun searchDog(breedName: String, withWait: Boolean = false) {
        searchJob?.cancel()
        searchJob = launchCoroutine {
            _state.value = _state.value?.copy(isLoading = true)
            if (withWait) {
                delay(debounceDelay)
            }
            val searchResults = mutableListOf<BreedDetails>()
            dogSearchUseCase.searchDog(breedName).map { breedDetails ->
                async {
                    breedDetails.imageId?.run {
                        val imageDetails = dogSearchUseCase.getImage(this)
                        searchResults.add(breedDetails.copy(imageDetails = imageDetails))
                    } ?: searchResults.add(breedDetails)
                }
            }.awaitAll()
            _state.value = _state.value?.copy(searchResults = searchResults, isLoading = false)
        }
    }

    fun onSearchTextChanged(searchString: String?, count: Int) {
        if (count > minimumCharacters && searchString?.isNotBlank() == true) {
            searchDog(searchString, true)
        }
    }

    override fun onCleared() {
        super.onCleared()
        searchJob?.cancel()
    }
}