package com.anuj.mvvmcleandemo.feature.details

import com.anuj.mvvmcleandemo.model.PclBreedDetails

data class BreedDetailsViewState (
    val breedDetails: PclBreedDetails?
)