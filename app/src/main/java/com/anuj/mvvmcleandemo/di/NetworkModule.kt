package com.anuj.mvvmcleandemo.di

import android.content.Context
import com.anuj.mvvmcleandemo.BuildConfig
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.anuj.mvvmcleandemo.data.network.DogService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object NetworkModule {

    @Provides
    fun provideClient(): OkHttpClient {
        val httpInterceptor = HttpLoggingInterceptor().setLevel(
            if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        )
        val headerInterceptor = object : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                val request = chain.request().newBuilder()
                    .addHeader("x-api-key", BuildConfig.API_KEY).build()
                return chain.proceed(request)
            }
        }
        return OkHttpClient.Builder().addNetworkInterceptor(headerInterceptor)
            .addInterceptor(httpInterceptor).build()
    }

    @Provides
    @Singleton
    fun provideRetrofitService(client: OkHttpClient): DogService {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.DOMAIN_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
            .create(DogService::class.java)
    }

    @Provides
    @Singleton
    fun providesGlideInstance(@ApplicationContext context: Context): RequestManager {
        return Glide.with(context)
    }
}