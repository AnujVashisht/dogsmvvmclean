package com.anuj.mvvmcleandemo.di

import com.anuj.mvvmcleandemo.domain.IDogSearchRepository
import com.anuj.mvvmcleandemo.data.DogSearchRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
abstract class RepositoryModule {
    @Binds
    abstract fun bindRepository(
        dogSearchRepository: DogSearchRepository
    ): IDogSearchRepository
}