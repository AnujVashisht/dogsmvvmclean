package com.anuj.mvvmcleandemo.di

import com.anuj.mvvmcleandemo.domain.DogSearchUseCase
import com.anuj.mvvmcleandemo.domain.IDogSearchRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@InstallIn(ViewModelComponent::class)
@Module
class UseCaseModule {
    @Provides
    fun provideUseCase(dogSearchUseCase: IDogSearchRepository) =
        DogSearchUseCase(dogSearchUseCase)
}