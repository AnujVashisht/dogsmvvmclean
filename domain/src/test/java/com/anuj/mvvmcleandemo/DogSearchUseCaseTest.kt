package com.anuj.mvvmcleandemo

import com.anuj.mvvmcleandemo.domain.DogSearchUseCase
import com.anuj.mvvmcleandemo.domain.IDogSearchRepository
import com.anuj.mvvmcleandemo.domain.domainmodel.ImageDetails
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class DogSearchUseCaseTest {

    private val searchRepository = mockk<IDogSearchRepository>()
    lateinit var dogSearchUseCase: DogSearchUseCase

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Before
    @ExperimentalCoroutinesApi
    fun setUp() {
        MockKAnnotations.init()
        dogSearchUseCase = DogSearchUseCase(searchRepository)
    }

    @Test
    fun `search dog should call repository method`() {
        coEvery { searchRepository.searchDog(any()) } returns emptyList()
        testCoroutineRule.runBlockingTest {
            dogSearchUseCase.searchDog("")
        }
        coVerify { searchRepository.searchDog(any()) }
    }

    @Test
    fun `get image should call repository method`() {
        coEvery { searchRepository.getImage(any()) } returns ImageDetails("test url")
        testCoroutineRule.runBlockingTest {
            dogSearchUseCase.getImage("")
        }
        coVerify { searchRepository.getImage(any()) }
    }
}