package com.anuj.mvvmcleandemo.domain.domainmodel

data class ImageDetails(
    val url: String
)
