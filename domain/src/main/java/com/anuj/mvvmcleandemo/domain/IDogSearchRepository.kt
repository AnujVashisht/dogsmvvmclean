package com.anuj.mvvmcleandemo.domain

import com.anuj.mvvmcleandemo.domain.domainmodel.BreedDetails
import com.anuj.mvvmcleandemo.domain.domainmodel.ImageDetails

interface IDogSearchRepository {
    suspend fun searchDog(breedName: String): List<BreedDetails>

    suspend fun getImage(imageId: String): ImageDetails
}