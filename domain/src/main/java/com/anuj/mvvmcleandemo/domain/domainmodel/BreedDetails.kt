package com.anuj.mvvmcleandemo.domain.domainmodel

data class BreedDetails(
    val id: String,
    val name: String,
    val temperament: String?,
    val lifeSpan: String,
    val imageId: String?,
    val imageDetails: ImageDetails? = null,
    val wikipediaUrl: String?,
    val origin: String?
)