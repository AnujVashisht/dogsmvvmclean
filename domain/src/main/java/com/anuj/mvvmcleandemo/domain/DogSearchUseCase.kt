package com.anuj.mvvmcleandemo.domain

import javax.inject.Inject

class DogSearchUseCase @Inject constructor(private val searchRepository: IDogSearchRepository) {

    suspend fun searchDog(breedName: String) = searchRepository.searchDog(breedName)

    suspend fun getImage(imageId: String) = searchRepository.getImage(imageId)
}