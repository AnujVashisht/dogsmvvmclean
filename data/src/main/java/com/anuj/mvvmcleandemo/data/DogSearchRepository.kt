package com.anuj.mvvmcleandemo.data

import com.anuj.mvvmcleandemo.domain.IDogSearchRepository
import com.anuj.mvvmcleandemo.domain.domainmodel.BreedDetails
import com.anuj.mvvmcleandemo.domain.domainmodel.ImageDetails
import com.anuj.mvvmcleandemo.data.model.toDomain
import com.anuj.mvvmcleandemo.data.network.DogService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DogSearchRepository @Inject constructor(private val service: DogService) :
    IDogSearchRepository {
    override suspend fun searchDog(breedName: String): List<BreedDetails> {
        return service.searchBreed(breedName).map { it.toDomain() }
    }

    override suspend fun getImage(imageId: String): ImageDetails {
        return service.getImage(imageId).toDomain()
    }
}