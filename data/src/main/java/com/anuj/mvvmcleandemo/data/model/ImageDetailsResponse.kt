package com.anuj.mvvmcleandemo.data.model

data class ImageDetailsResponse(
    val url: String
)
