package com.anuj.mvvmcleandemo.data.network

import com.anuj.mvvmcleandemo.data.model.BreedDetailsResponse
import com.anuj.mvvmcleandemo.data.model.ImageDetailsResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface DogService {
    @GET("breeds")
    suspend fun getAllBreeds(
        @Query("limit") limit: String,
        @Query("page") page: String
    ): List<BreedDetailsResponse>

    @GET("breeds/search")
    suspend fun searchBreed(@Query("q") breedName: String): List<BreedDetailsResponse>

    @GET("images/{image_id}")
    suspend fun getImage(@Path("image_id") imageId: String): ImageDetailsResponse
}