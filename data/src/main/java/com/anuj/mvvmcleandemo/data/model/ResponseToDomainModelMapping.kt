package com.anuj.mvvmcleandemo.data.model

import com.anuj.mvvmcleandemo.domain.domainmodel.BreedDetails
import com.anuj.mvvmcleandemo.domain.domainmodel.ImageDetails

fun BreedDetailsResponse.toDomain() = BreedDetails(
    id, name, temperament, lifeSpan, imageId, wikipediaUrl = wikipediaUrl, origin = origin
)

fun ImageDetailsResponse.toDomain() = ImageDetails(url)