package com.anuj.mvvmcleandemo.data.model

import com.google.gson.annotations.SerializedName

data class BreedDetailsResponse(
    val id: String,
    val name: String,
    val temperament: String?,
    @SerializedName("life_span") val lifeSpan: String,
    @SerializedName("reference_image_id") val imageId: String?,
    @SerializedName("wikipedia_url") val wikipediaUrl: String?,
    @SerializedName("origin") val origin: String?
)