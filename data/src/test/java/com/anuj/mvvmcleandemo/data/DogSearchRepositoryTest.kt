package com.anuj.mvvmcleandemo.data

import com.anuj.mvvmcleandemo.TestCoroutineRule
import com.anuj.mvvmcleandemo.data.model.ImageDetailsResponse
import com.anuj.mvvmcleandemo.data.network.DogService
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class DogSearchRepositoryTest {

    private val dogService = mockk<DogService>()
    private lateinit var dogSearchRepository: DogSearchRepository

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Before
    @ExperimentalCoroutinesApi
    fun setUp() {
        MockKAnnotations.init()
        dogSearchRepository = DogSearchRepository(dogService)
    }

    @Test
    fun `when search breed repository method is called, should trigger service call`() {
        coEvery { dogService.searchBreed(any()) } returns emptyList()
        testCoroutineRule.runBlockingTest {
            dogSearchRepository.searchDog("")
        }
        coVerify { dogService.searchBreed(any()) }
    }

    @Test
    fun `when get image repository method is called, should trigger service call`() {
        coEvery { dogService.getImage(any()) } returns ImageDetailsResponse("")
        testCoroutineRule.runBlockingTest {
            dogSearchRepository.getImage("")
        }
        coVerify { dogService.getImage(any()) }
    }
}